<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->login();
	}

	public function login()
	{
		$this->load->view('login');
	}

	public function logout(){

		$this->session->sess_destroy();
		redirect('main/login');
	}

	public function signup(){

		$this->load->view('signup');
	}

	public function members(){
	
		if($this->session->userdata('is_logged_in')){
			$this->load->view('members');
	
		}else{
			redirect('main/restricted');
		}
	}

	public function restricted(){
		$this->load->view('restricted');
	}

	public function login_validation()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email', 'Email', 'required|trim|xss_clean|callback_validate_credentials');
		$this->form_validation->set_rules('password', 'Password', 'required|md5');

		if($this->form_validation->run()){

			$data = array(
				'email' => $this->input->post('email'),
				'is_logged_in' => 1
				);
			$this->session->set_userdata($data);
			redirect('main/members');
		}
		else{
			$this->load->view('login');
		}

	}

	public function signup_validation(){
		$this->load->library('form_validation');


		$this->form_validation->set_rules('email', 'Email', 'requred|trim|valid_email|is_unique[users.email]');

		$this->form_validation->set_rules('password', 'Password', 'required|trim');

		$this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|trim|matches[password]');



		if($this->form_validation->run()){
				
				$key = md5(uniqid());

				$this->load->library('email', array('mailtype' => 'html'));

				$this->email->from('bojorquebryan@gmail.com', 'Bryan');
				$this->email->to($this->input->post('email'));
				$this->email->subject('Confirm your account');
				
				$message = '<p>Thank you for signing up.</p>';
				$message .= "<p><a href='".base_url()."main/register_user/'".$key."' '>Click here to confirm yout account.</a></p>";


				$this->email->message($message);
				if ($this->email->send()) {
					# code...
					echo 'The email has been sent.';
				}else{
					echo 'Email sending failed.';
				}

		}else{
			$this->load->view('signup');	
		}
	}

	public function validate_credentials(){
		$this->load->model('model_users');
		
		if($this->model_users->can_log_in()){
			return true;
		}else{
			$this->form_validation->set_message('validate_credentials', 'Incorrect username/password');
			return false;
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */